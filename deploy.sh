#!/bin/bash
docker network create cct_network


#запуск gateway
docker compose up -d

#запуск сервиса user
cd ../cct-user-microservice
docker compose up -d

# запуск сервиса auth
cd ../cct-auth-microservice
docker compose up -d

# запуск сервиса auth
cd ../cct-exchange-microservice
docker compose up -d

