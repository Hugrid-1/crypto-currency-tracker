package modules

import (
	"gitlab.com/Hugrid-1/crypto-currency-tracker/internal/infrastructure/component"
	acontroller "gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules/auth/controller"
	excontroller "gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules/exchange/controller"
	ucontroller "gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules/user/controller"
)

type Controllers struct {
	Auth     acontroller.Auther
	User     ucontroller.Userer
	Exchange excontroller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	exchangeController := excontroller.NewExchange(services.Exchange, components)

	return &Controllers{
		Auth:     authController,
		User:     userController,
		Exchange: exchangeController,
	}
}
