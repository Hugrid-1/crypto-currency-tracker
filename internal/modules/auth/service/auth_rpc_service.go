package service

import (
	"context"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
)

type AuthServiceRPC struct {
	auth interfaces.Auther
}

// NewAuthJSONRPCService возвращает новый AuthServiceRPC
func NewAuthRPCService(Auth interfaces.Auther) *AuthServiceRPC {
	return &AuthServiceRPC{auth: Auth}
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод Register из Auth.
// Возвращает выходные данные.
func (s *AuthServiceRPC) Register(ctx context.Context, in interfaces.RegisterIn) interfaces.RegisterOut {
	out := s.auth.Register(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizeEmail из Auth.
// Возвращает выходные данные.
func (s *AuthServiceRPC) AuthorizeEmail(ctx context.Context, in interfaces.AuthorizeEmailIn) interfaces.AuthorizeOut {
	out := s.auth.AuthorizeEmail(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizeRefresh из Auth.
// Возвращает выходные данные.
func (s *AuthServiceRPC) AuthorizeRefresh(ctx context.Context, in interfaces.AuthorizeRefreshIn) interfaces.AuthorizeOut {
	out := s.auth.AuthorizeRefresh(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizePhone из Auth.
// Возвращает выходные данные.
func (s *AuthServiceRPC) AuthorizePhone(ctx context.Context, in interfaces.AuthorizePhoneIn) interfaces.AuthorizeOut {
	out := s.auth.AuthorizePhone(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод SendPhoneCode из Auth.
// Возвращает выходные данные.
func (s *AuthServiceRPC) SendPhoneCode(ctx context.Context, in interfaces.SendPhoneCodeIn) interfaces.SendPhoneCodeOut {
	out := s.auth.SendPhoneCode(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод VerifyEmail из Auth.
// Возвращает выходные данные.
func (s *AuthServiceRPC) VerifyEmail(ctx context.Context, in interfaces.VerifyEmailIn) interfaces.VerifyEmailOut {
	out := s.auth.VerifyEmail(ctx, in)
	return out
}
