package modules

import (
	"fmt"
	auth_interface "gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	authGRPC "gitlab.com/Hugrid-1/cct-auth-microservice/rpc/grpc"
	authJRPC "gitlab.com/Hugrid-1/cct-auth-microservice/rpc/jrpc"
	exchange_interface "gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
	exchangeGRPC "gitlab.com/Hugrid-1/cct-exchange-microservice/rpc/grpc"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	userGRPC "gitlab.com/Hugrid-1/cct-user-microservice/rpc/grpc"
	userJRPC "gitlab.com/Hugrid-1/cct-user-microservice/rpc/jrpc"
	"gitlab.com/Hugrid-1/crypto-currency-tracker/internal/infrastructure/component"
	aservice "gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules/auth/service"
	exservice "gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules/exchange/service"
	uservice "gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules/user/service"
	"log"
)

type Services struct {
	User     user_interface.Userer
	Auth     auth_interface.Auther
	Exchange exchange_interface.Exchanger
}

func NewServices(components *component.Components) *Services {
	// Определение переменных для RPC сервисов
	var userRPCService *uservice.UserServiceRPC
	var authRPCService *aservice.AuthServiceRPC
	var exchangeRPCService *exservice.ExchangeServiceRPC

	// Получение адресов микросервисов
	userRPCAddress := fmt.Sprintf("%s:%s", components.Conf.UserRPC.Host, components.Conf.UserRPC.Port)
	authRPCAddress := fmt.Sprintf("%s:%s", components.Conf.AuthRPC.Host, components.Conf.AuthRPC.Port)
	exchageRPCAddress := fmt.Sprintf("%s:%s", components.Conf.ExchangeRPC.Host, components.Conf.ExchangeRPC.Port)

	// Определение типа RPC серверов
	switch components.Conf.RPCServer.Type {
	case "grpc":
		userClient, err := userGRPC.NewUserGRPCClient(userRPCAddress)
		authClient, err := authGRPC.NewAuthGRPCClient(authRPCAddress)
		exchangeClient, err := exchangeGRPC.NewExchangeGRPCClient(exchageRPCAddress)
		if err != nil {
			log.Printf("Error creating user RPC client: %v", err)
		}
		userRPCService = uservice.NewUserRPCService(userClient)
		authRPCService = aservice.NewAuthRPCService(authClient)
		exchangeRPCService = exservice.NewExchangeServiceRPC(exchangeClient)
	case "jsonrpc":
		userClient, err := userJRPC.NewUserJRPCClient(userRPCAddress)
		authClient, err := authJRPC.NewAuthClient(authRPCAddress)
		exchangeClient, err := exchangeGRPC.NewExchangeGRPCClient(exchageRPCAddress)
		if err != nil {
			log.Printf("Error creating user RPC client: %v", err)
		}
		userRPCService = uservice.NewUserRPCService(userClient)
		authRPCService = aservice.NewAuthRPCService(authClient)
		exchangeRPCService = exservice.NewExchangeServiceRPC(exchangeClient)
	default:
		components.Logger.Fatal("error: bad rpc type ( check user .env file )")
	}
	return &Services{
		User:     userRPCService,
		Auth:     authRPCService,
		Exchange: exchangeRPCService,
	}
}
