package service

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
)

type ExchangeServiceRPC struct {
	exchangeService interfaces.Exchanger
}

func NewExchangeServiceRPC(exchangeService interfaces.Exchanger) *ExchangeServiceRPC {
	u := &ExchangeServiceRPC{exchangeService: exchangeService}

	return u
}

func (ex *ExchangeServiceRPC) GetPairs(ctx context.Context) messages.ExchangeHistoryOut {
	return ex.exchangeService.GetPairs(ctx)
}

func (ex *ExchangeServiceRPC) GetMinPairs(ctx context.Context) messages.ExchangeOut {
	return ex.exchangeService.GetMinPairs(ctx)
}

func (ex *ExchangeServiceRPC) GetMaxPairs(ctx context.Context) messages.ExchangeOut {
	return ex.exchangeService.GetMaxPairs(ctx)
}

func (ex *ExchangeServiceRPC) GetAveragePairs(ctx context.Context) messages.ExchangeOut {
	return ex.exchangeService.GetAveragePairs(ctx)
}

func (ex *ExchangeServiceRPC) GetTicker(ctx context.Context) messages.ExchangeOut {
	return ex.exchangeService.GetTicker(ctx)
}
func (ex *ExchangeServiceRPC) GetPairNames(ctx context.Context) messages.ExchangePairsListOut {
	return ex.exchangeService.GetPairNames(ctx)
}
