package user

import (
	"context"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
)

// UserServiceJSONRPC представляет UserService для использования в JSON-RPC
type UserServiceRPC struct {
	userService user_interface.Userer
}

// NewUserRPCService возвращает новый UserServiceJSONRPC
func NewUserRPCService(userService user_interface.Userer) *UserServiceRPC {
	return &UserServiceRPC{userService: userService}
}

// Create обрабатывает JSON-RPC запрос на создание пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод Create из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) Create(ctx context.Context, in user_interface.UserCreateIn) user_interface.UserCreateOut {
	out := t.userService.Create(ctx, in)
	return out
}

// Update обрабатывает JSON-RPC запрос на обновление пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод Update из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) Update(ctx context.Context, in user_interface.UserUpdateIn) user_interface.UserUpdateOut {
	out := t.userService.Update(ctx, in)
	return out
}

// VerifyEmail обрабатывает JSON-RPC запрос на подтверждение email пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод VerifyEmail из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) VerifyEmail(ctx context.Context, in user_interface.UserVerifyEmailIn) user_interface.UserUpdateOut {
	out := t.userService.VerifyEmail(ctx, in)
	return out
}

// ChangePassword обрабатывает JSON-RPC запрос на смену пароля пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод ChangePassword из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) ChangePassword(ctx context.Context, in user_interface.ChangePasswordIn) user_interface.ChangePasswordOut {
	out := t.userService.ChangePassword(ctx, in)
	return out
}

// GetByEmail обрабатывает JSON-RPC запрос на получение пользователя по email.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByEmail из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) GetByEmail(ctx context.Context, in user_interface.GetByEmailIn) user_interface.UserOut {
	out := t.userService.GetByEmail(ctx, in)
	return out
}

// GetByPhone обрабатывает JSON-RPC запрос на получение пользователя по телефону.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByPhone из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) GetByPhone(ctx context.Context, in user_interface.GetByPhoneIn) user_interface.UserOut {
	out := t.userService.GetByPhone(ctx, in)
	return out
}

// GetByID обрабатывает JSON-RPC запрос на получение пользователя по ID.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByID из userService.
// Возвращает выходные данные.
func (t *UserServiceRPC) GetByID(ctx context.Context, in user_interface.GetByIDIn) user_interface.UserOut {
	out := t.userService.GetByID(ctx, in)
	return out
}

// GetByIDs обрабатывает JSON-RPC запрос на получение пользователей по списку ID.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод
// GetByIDs из userService. Возвращает выходные данные.
func (t *UserServiceRPC) GetByIDs(ctx context.Context, in user_interface.GetByIDsIn) user_interface.UsersOut {
	out := t.userService.GetByIDs(ctx, in)
	return out
}
