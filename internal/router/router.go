package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/Hugrid-1/crypto-currency-tracker/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/crypto-currency-tracker/internal/infrastructure/middleware"
	"gitlab.com/Hugrid-1/crypto-currency-tracker/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/auth", func(r chi.Router) {
				authController := controllers.Auth
				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				r.Post("/verify", authController.Verify)
			})
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
					r.Post("/password_change", userController.ChangePassword)
				})
			})
			r.Route("/exchange", func(r chi.Router) {
				exchangeController := controllers.Exchange
				r.Get("/ticker", exchangeController.GetTicker)
				r.Get("/history", exchangeController.GetHistory)
				r.Get("/min", exchangeController.GetMaxPairs)
				r.Get("/max", exchangeController.GetMinPairs)
				r.Get("/avg", exchangeController.GetAvgPairs)
				r.Get("/names", exchangeController.GetPairsNameList)

			})
		})
	})

	return r
}
