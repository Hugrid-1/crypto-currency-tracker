package bot

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	rabbitMQ "github.com/rabbitmq/amqp091-go"
	"gitlab.com/Hugrid-1/crypto-currency-tracker/config"
	"go.uber.org/zap"
	"sync"
)

type TelegramBot struct {
	bot          *tgbotapi.BotAPI
	logger       *zap.Logger
	rabbitConfig config.RabbitMQ
	rb           *RabbitMQ
	subscribers  map[int64]string
	sync.RWMutex
}

type RabbitMQ struct {
	conn *rabbitMQ.Connection
	ch   *rabbitMQ.Channel
}

func NewTelegramBot(logger *zap.Logger, conf config.AppConf) *TelegramBot {
	bot, err := tgbotapi.NewBotAPI(conf.TelegramToken)
	if err != nil {
		logger.Error("TelegramBot init error")
		return nil
	}
	logger.Info("TelegramBot init success")
	subscribers := make(map[int64]string)

	return &TelegramBot{rabbitConfig: conf.RabbitMQ, logger: logger, bot: bot, subscribers: subscribers, rb: &RabbitMQ{
		conn: nil,
		ch:   nil,
	}}
}

func (tb *TelegramBot) Run(ctx context.Context) error {
	go tb.Update(ctx)
	return tb.SubscribeRabbitMQ(ctx)
}

func (tb *TelegramBot) SendToSubscribers(message []byte) {
	for userID := range tb.subscribers {
		msg := tgbotapi.NewMessage(userID, string(message))
		_, err := tb.bot.Send(msg)

		if err != nil {
			tb.logger.Error("failed to send message.", zap.Error(err))
			delete(tb.subscribers, userID)
		}
	}
}

func (tb *TelegramBot) Update(ctx context.Context) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := tb.bot.GetUpdatesChan(u)
	if err != nil {
		tb.logger.Error("Failed to get updates: %s", zap.Error(err))
	}

	var update tgbotapi.Update

	for {
		select {
		case <-ctx.Done():
			return

		case update = <-updates:
			if update.Message == nil {
				continue
			}
			tb.Lock()
			tb.subscribers[update.Message.Chat.ID] = update.Message.From.UserName
			tb.Unlock()
		}
	}

}

func (tb *TelegramBot) SubscribeRabbitMQ(ctx context.Context) error {

	connStr := fmt.Sprintf("amqp://%s:%s@%s:%s/", tb.rabbitConfig.User, tb.rabbitConfig.Password, tb.rabbitConfig.Host, tb.rabbitConfig.Port)
	var err error
	tb.rb.conn, err = rabbitMQ.Dial(connStr)
	if err != nil {
		tb.logger.Error("unable to open connect to RabbitMQ server. Error: %s", zap.Error(err))
		return err
	}

	defer func() {
		_ = tb.rb.conn.Close() // Закрываем подключение в случае удачной попытки подключения
	}()

	tb.rb.ch, err = tb.rb.conn.Channel()
	if err != nil {
		tb.logger.Error("failed to open a channel. Error: %s", zap.Error(err))
		return err
	}

	defer func() {
		_ = tb.rb.ch.Close() // Закрываем подключение в случае удачной попытки подключения
	}()

	q, err := tb.rb.ch.QueueDeclare(
		"exchange", // name
		false,      // durable
		false,      // delete when unused
		false,      // exclusive
		false,      // no-wait
		nil,        // arguments
	)
	if err != nil {
		tb.logger.Error("failed to declare a queue. Error: %s", zap.Error(err))
		return err
	}

	for {
		messages, err := tb.rb.ch.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		if err != nil {
			tb.logger.Error("failed to register a consumer. Error: %s", zap.Error(err))
		}

		for message := range messages {
			tb.SendToSubscribers(message.Body)
		}
	}
	return err
}
