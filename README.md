# Crypto currency tracker

# Links
User https://gitlab.com/Hugrid-1/cct-user-microservice \
Auth https://gitlab.com/Hugrid-1/cct-auth-microservice \
Exchange https://gitlab.com/Hugrid-1/cct-exchange-microservice

# Ports
Gateway     | PORT 8080 \
User service | PORT 8081 | REDIS 6381 | POSTGRES 5432 | \
Auth service | PORT 8082 | REDIS 6382 | POSTGRES 5433 | \
Exchange service | PORT 8083 | REDIS 6383 | POSTGRES 5434 |

# Start
````
deploy.sh 
````





